//! This module contains traits that are usable with `#[derive(...)].`

use crate::{App, ArgMatches, Error};

use futures::future::LocalBoxFuture;
use librust::ffi::OsString;

/// This trait is just a convenience on top of FromArgMatches + IntoApp
pub trait Clap: FromArgMatches + IntoApp + Sized {
    /// Parse from `std::env::args()`, exit on error
    fn parse<'a>() -> LocalBoxFuture<'a, Self> {
        Box::pin(async move {
            let matches = <Self as IntoApp>::into_app().get_matches().await;
            <Self as FromArgMatches>::from_arg_matches(&matches)
        })
    }

    /// Parse from `std::env::args()`, return Err on error.
    fn try_parse<'a>() -> LocalBoxFuture<'a, Result<Self, Error>> {
        Box::pin(async move {
            let matches = <Self as IntoApp>::into_app().try_get_matches().await?;
            Ok(<Self as FromArgMatches>::from_arg_matches(&matches))
        })
    }

    /// Parse from iterator, exit on error
    fn parse_from<'a, I, T>(itr: I) -> LocalBoxFuture<'a, Self>
    where
        I: IntoIterator<Item = T> + 'a,
        // TODO (@CreepySkeleton): discover a way to avoid cloning here
        T: Into<OsString> + Clone,
    {
        Box::pin(async move {
            let matches = <Self as IntoApp>::into_app().get_matches_from(itr).await;
            <Self as FromArgMatches>::from_arg_matches(&matches)
        })
    }

    /// Parse from iterator, return Err on error.
    fn try_parse_from<'a, I, T>(itr: I) -> LocalBoxFuture<'a, Result<Self, Error>>
    where
        I: IntoIterator<Item = T> + 'a,
        // TODO (@CreepySkeleton): discover a way to avoid cloning here
        T: Into<OsString> + Clone,
    {
        Box::pin(async move {
            let matches = <Self as IntoApp>::into_app()
                .try_get_matches_from(itr)
                .await?;
            Ok(<Self as FromArgMatches>::from_arg_matches(&matches))
        })
    }
}

/// Build an App according to the struct
///
/// Also serves for flattening
pub trait IntoApp: Sized {
    /// @TODO @release @docs
    fn into_app<'help>() -> App<'help>;
    /// @TODO @release @docs
    fn augment_clap(app: App<'_>) -> App<'_>;
}

/// Extract values from ArgMatches into the struct.
pub trait FromArgMatches: Sized {
    /// @TODO @release @docs
    fn from_arg_matches(matches: &ArgMatches) -> Self;
}

/// @TODO @release @docs
pub trait Subcommand: Sized {
    /// @TODO @release @docs
    fn from_subcommand(name: &str, matches: Option<&ArgMatches>) -> Option<Self>;
    /// @TODO @release @docs
    fn augment_subcommands(app: App<'_>) -> App<'_>;
}

/// @TODO @release @docs
pub trait ArgEnum: Sized {
    /// @TODO @release @docs
    const VARIANTS: &'static [&'static str];

    /// @TODO @release @docs
    fn from_str(input: &str, case_insensitive: bool) -> Result<Self, String>;
}

impl<T: Clap> Clap for Box<T> {
    fn parse<'a>() -> LocalBoxFuture<'a, Self> {
        Box::pin(async move { Box::new(<T as Clap>::parse().await) })
    }

    fn try_parse<'a>() -> LocalBoxFuture<'a, Result<Self, Error>> {
        Box::pin(async move { <T as Clap>::try_parse().await.map(Box::new) })
    }

    fn parse_from<'a, I, It>(itr: I) -> LocalBoxFuture<'a, Self>
    where
        I: IntoIterator<Item = It> + 'a,
        // TODO (@CreepySkeleton): discover a way to avoid cloning here
        It: Into<OsString> + Clone,
    {
        Box::pin(async move { Box::new(<T as Clap>::parse_from(itr).await) })
    }

    fn try_parse_from<'a, I, It>(itr: I) -> LocalBoxFuture<'a, Result<Self, Error>>
    where
        I: IntoIterator<Item = It> + 'a,
        // TODO (@CreepySkeleton): discover a way to avoid cloning here
        It: Into<OsString> + Clone,
    {
        Box::pin(async move {
            <T as Clap>::try_parse_from(itr).await.map(Box::new)
        })
    }
}

impl<T: IntoApp> IntoApp for Box<T> {
    fn into_app<'help>() -> App<'help> {
        <T as IntoApp>::into_app()
    }
    fn augment_clap(app: App<'_>) -> App<'_> {
        <T as IntoApp>::augment_clap(app)
    }
}

impl<T: FromArgMatches> FromArgMatches for Box<T> {
    fn from_arg_matches(matches: &ArgMatches) -> Self {
        Box::new(<T as FromArgMatches>::from_arg_matches(matches))
    }
}

impl<T: Subcommand> Subcommand for Box<T> {
    fn from_subcommand(name: &str, matches: Option<&ArgMatches>) -> Option<Self> {
        <T as Subcommand>::from_subcommand(name, matches).map(Box::new)
    }
    fn augment_subcommands(app: App<'_>) -> App<'_> {
        <T as Subcommand>::augment_subcommands(app)
    }
}
