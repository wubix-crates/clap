use crate::util::termcolor::{Buffer, BufferWriter, ColorChoice};
#[cfg(feature = "color")]
use crate::util::termcolor::{Color, ColorSpec, WriteColor};

use librust::io::{PollWrite, Result, Write};
use std::{
    fmt::{self, Debug, Formatter},
    pin::Pin,
};

#[cfg(feature = "color")]
fn is_a_tty(stderr: bool) -> bool {
    debug!("is_a_tty: stderr={:?}", stderr);

    let stream = if stderr {
        atty::Stream::Stderr
    } else {
        atty::Stream::Stdout
    };

    atty::is(stream)
}

#[cfg(not(feature = "color"))]
fn is_a_tty(_: bool) -> bool {
    debug!("is_a_tty");
    false
}

pub(crate) struct Colorizer {
    writer: BufferWriter,
    buffer: Buffer,
}

impl Debug for Colorizer {
    #[cfg(feature = "color")]
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", String::from_utf8_lossy(self.buffer.as_slice()))
    }

    #[cfg(not(feature = "color"))]
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", String::from_utf8_lossy(&self.buffer))
    }
}

impl Colorizer {
    pub(crate) fn new(use_stderr: bool, when: ColorChoice) -> Self {
        let checked_when = if is_a_tty(use_stderr) {
            when
        } else {
            ColorChoice::Never
        };

        let writer = if use_stderr {
            BufferWriter::stderr(checked_when)
        } else {
            BufferWriter::stdout(checked_when)
        };

        let buffer = writer.buffer();

        Self { writer, buffer }
    }

    pub(crate) async fn print(&self) -> Result<()> {
        self.writer.print(&self.buffer).await
    }

    #[cfg(feature = "color")]
    pub(crate) async fn good(&mut self, msg: &str) -> Result<()> {
        self.buffer
            .set_color(ColorSpec::new().set_fg(Some(Color::Green)))
            .await?;
        self.write_all(msg.as_bytes()).await?;
        self.buffer.reset().await
    }

    #[cfg(not(feature = "color"))]
    pub(crate) fn good(&mut self, msg: &str) -> Result<()> {
        self.none(msg)
    }

    #[cfg(feature = "color")]
    pub(crate) async fn warning(&mut self, msg: &str) -> Result<()> {
        self.buffer
            .set_color(ColorSpec::new().set_fg(Some(Color::Yellow)))
            .await?;
        self.write_all(msg.as_bytes()).await?;
        self.buffer.reset().await
    }

    #[cfg(not(feature = "color"))]
    pub(crate) fn warning(&mut self, msg: &str) -> Result<()> {
        self.none(msg)
    }

    #[cfg(feature = "color")]
    pub(crate) async fn error(&mut self, msg: &str) -> Result<()> {
        self.buffer
            .set_color(ColorSpec::new().set_fg(Some(Color::Red)).set_bold(true))
            .await?;
        self.write_all(msg.as_bytes()).await?;
        self.buffer.reset().await
    }

    #[cfg(not(feature = "color"))]
    pub(crate) fn error(&mut self, msg: &str) -> Result<()> {
        self.none(msg)
    }

    pub(crate) async fn none(&mut self, msg: &str) -> Result<()> {
        self.write_all(msg.as_bytes()).await?;
        Ok(())
    }
}

impl PollWrite for Colorizer {
    fn poll_write(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &[u8],
    ) -> std::task::Poll<Result<usize>> {
        Pin::new(&mut self.buffer).poll_write(cx, buf)
    }

    fn poll_flush(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<()>> {
        Pin::new(&mut self.buffer).poll_flush(cx)
    }

    fn poll_close(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<()>> {
        Pin::new(&mut self.buffer).poll_close(cx)
    }
}
