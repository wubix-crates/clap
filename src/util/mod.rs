#![allow(clippy::single_component_path_imports)]

mod argstr;
mod fnv;
mod graph;
mod id;

pub use self::fnv::Key;

pub(crate) use self::{argstr::ArgStr, graph::ChildGraph, id::Id};
pub(crate) use vec_map::VecMap;

#[cfg(feature = "color")]
pub(crate) use termcolor;

#[cfg(not(feature = "color"))]
pub(crate) mod termcolor;

pub(crate) async fn safe_exit(code: i32) -> ! {
    use librust::io::Write;

    let _ = librust::io::stdout().lock().flush().await;
    let _ = librust::io::stderr().lock().flush().await;

    librust::process::exit(code)
}
